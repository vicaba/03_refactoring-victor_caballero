DROP DATABASE IF EXISTS coan_secure;

CREATE DATABASE coan_secure;

USE coan_secure;

CREATE TABLE IF NOT EXISTS `users` (
  `id`       INT(11)     NOT NULL AUTO_INCREMENT,
  `email`    VARCHAR(32) NOT NULL,
  `password` CHAR(60)    NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1
  AUTO_INCREMENT = 1;