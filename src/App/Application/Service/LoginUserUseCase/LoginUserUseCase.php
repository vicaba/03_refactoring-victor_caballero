<?php

namespace App\Application\Service\LoginUserUseCase;

use App\Domain\Repository\RepositoryException;
use App\Domain\Repository\UserRepository;
use App\Domain\Service\Crypt\Hasher;

class LoginUserUseCase
{
    private $userRepository;

    private $hasher;

    public function __construct(UserRepository $userRepository, Hasher $hasher)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
    }

    public function execute(LoginUserRequest $request)
    {
        try {

            $user = $this->userRepository->getUserByEmail($request->getEmail());
            if (!isset($user)) {
                return false;
            }
            $isUserRegistered = $this->verifyPassword($request->getPassword(), $user->getPassword());

            if (!$isUserRegistered) return false;

            return $user;

        } catch (RepositoryException $re) {
            return false;
        }

    }

    private function verifyPassword($password, $hash) {
        return $this->hasher->verify($password, $hash);
    }

}