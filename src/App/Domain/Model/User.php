<?php

namespace App\Domain\Model;


class User
{

    private $id;

    private $email;

    private $password;

    public function __construct($id, $email, $password) {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        return new User($id, $this->email, $this->password);
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        return new User($this->id, $email, $this->password);
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        return new User($this->id, $this->email, $password);
    }

}