<?php

namespace App\Domain\Repository;


interface UserRepository extends Repository
{

    public function saveUser(SaveUserRequest $user);

    public function getUserByEmail($email);

}