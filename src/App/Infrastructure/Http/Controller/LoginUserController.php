<?php

namespace App\Infrastructure\Http\Controller;


use App\Application\Service\LoginUserUseCase\LoginUserRequest;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class LoginUserController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $userLoginControllers = $app["controllers_factory"];

        $userLoginControllers->get("/", array($this, "login"))->bind("login");
        $userLoginControllers->post("/", array($this, "loginSubmit"))->bind("login_submit");

        return $userLoginControllers;
    }

    public function login(Application $app)
    {
        return $app['twig']->render('login.twig');
    }

    public function loginSubmit(Application $app, Request $request)
    {
        $loginUserUseCase = $app["login_use_case"];
        $isUserRegistered = $loginUserUseCase->execute(
            new LoginUserRequest(
                $request->request->get("email"),
                $request->request->get("password")
            ));
        if (!$isUserRegistered) {
            return $app['twig']->render('login_failure_response.twig');
        }
        return $app['twig']->render('login_success_response.twig');

    }
}