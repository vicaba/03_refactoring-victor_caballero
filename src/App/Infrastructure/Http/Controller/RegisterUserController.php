<?php

namespace App\Infrastructure\Http\Controller;


use App\Application\Service\RegisterUserUseCase\RegisterUserRequest;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class RegisterUserController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $userRegistrationControllers = $app["controllers_factory"];

        $userRegistrationControllers->get("/", array($this, "register"))->bind("register");
        $userRegistrationControllers->post("/", array($this, "registerSubmit"))->bind("register_submit");

        return $userRegistrationControllers;
    }

    public function register(Application $app)
    {
        return $app['twig']->render('register.twig');
    }

    public function registerSubmit(Application $app, Request $request)
    {

        $registerUserUseCase = $app["register_use_case"];
        $response = $registerUserUseCase->execute(
            new RegisterUserRequest(
                $request->request->get("email"),
                $request->request->get("password")
            ));
        if (!$response) {
            return $app['twig']->render('register_failure_response.twig');
        }
        return $app['twig']->render('register_success_response.twig');

    }
}