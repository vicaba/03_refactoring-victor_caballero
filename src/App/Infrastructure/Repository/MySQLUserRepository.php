<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\User;
use App\Domain\Repository\RepositoryException;
use App\Domain\Repository\SaveUserRequest;
use App\Domain\Repository\UserRepository;
use PDO;

class MySQLUserRepository implements UserRepository
{
    private $connector;

    const USER_ID_KEY = "id";

    const USER_EMAIL_KEY = "email";

    const USER_PASSWORD_KEY = "password";

    public function __construct($conn)
    {
        $this->connector = $conn;
    }

    public function saveUser(SaveUserRequest $saveUserRequest)
    {
        $query = "INSERT INTO users SET email = ?, password = ?";
        $stmt = $this->connector->prepare($query);

        $stmt->bindParam(1, $saveUserRequest->email);
        $stmt->bindParam(2, $saveUserRequest->password);

        if (!$stmt->execute()) {
            throw new RepositoryException("Unable to save user.");
        }
    }

    public function getUserByEmail($email)
    {
        $query = "SELECT id, email, password FROM users WHERE email = ? LIMIT 0, 1";
        $stmt = $this->connector->prepare($query);

        $stmt->bindParam(1, $email);

        if (!$stmt->execute()) {
            throw new RepositoryException("Unable to get user.");
        }

        $num = $stmt->rowCount();

        if ($num == 1) {

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return new User($row[self::USER_ID_KEY], $row[self::USER_EMAIL_KEY], $row[self::USER_PASSWORD_KEY]);
        }

        return null;
    }
}